# Exploring the integration of CLJS, Shadow-CLJS, UIx2 and Material UI

## Dependencies

This project depends on:

- `ClojureScript`
  - https://clojurescript.org/
- `shadow-cljs`
  - https://github.com/thheller/shadow-cljs
- `UIx²` (Idiomatic ClojureScript interface to modern React.js)
  - https://github.com/pitch-io/uix
- `Material UI`
  - https://mui.com/

## Development

### Install dependencies

```bash
npm install
```

or

```bash
yarn install
```

### Start dev environment

```bash
npx shadow-cljs watch frontend
```

if everything is ok you'll see something like this:

```
shadow-cljs - config: .../cljs-material-ui/shadow-cljs.edn
shadow-cljs - HTTP server available at http://localhost:8080
shadow-cljs - server version: 2.20.20 running at http://localhost:9630
shadow-cljs - nREPL server started on port 41283
shadow-cljs - watching build :frontend
[:frontend] Configuring build.
[:frontend] Compiling ...
[:frontend] Build completed. (978 files, 977 compiled, 0 warnings, 32,13s)
```
