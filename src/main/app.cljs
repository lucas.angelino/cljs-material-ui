(ns app
  (:require [uix.core :as uix :refer [$ defui]]
            [uix.dom]
            ["@mui/material" :as material]
            ["@mui/material/styles" :as styles]
            ["@mui/icons-material/Send$default" :as SendIcon]))

(defn change-mode [current-mode]
  (if (= current-mode "light") "dark" "light"))

(defui app []
  (let [[state set-state!] (uix/use-state {:theme {:palette {:mode "dark"}}})]
    ($ styles/ThemeProvider {:theme (styles/createTheme (clj->js (:theme state)))}
       ($ material/CssBaseline)
       ($ material/Grid
          {:style {:width 200}}
          ($ material/Stack
             {:spacing 2}
             ($ material/Button
                {:variant "contained"
                 :on-click #(set-state! (update-in state [:theme :palette :mode] change-mode))}
                "change theme")
             ($ material/Button
                {:variant "contained"
                 :disableElevation true
                 :on-click #(js/alert "pressed!")}
                "press me")
             ($ material/Button
                {:variant "contained"
                 :start-icon ($ SendIcon)
                 :on-click #(js/console.log "pressed!")}
                "press me"))))))

(defonce root (uix.dom/create-root (js/document.getElementById "root")))

(defn init []
  (uix.dom/render-root ($ app) root)
  (println "App Started."))